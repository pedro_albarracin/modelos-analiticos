# -*- coding: utf-8 -*-
################################################################################
# Import libraries
################################################################################
from clustering_core import *
import sys
import inspect, os

################################################################################
# Get config path, parameters path, session ID
################################################################################
variablesPath = os.path.abspath(argv[1])
environmentPath = os.path.abspath(argv[2])
modelID = sys.argv[3]
sessionID = sys.argv[4]

#INTRODUCE EL DICCIONARIO DE LAS VARIABLES DE LOS CLIENTES
dict_vars = {
	'co_cliente': [1000, 1001, 1002, 1003, 1004, 1005, 1006],
	'Sexo': [0, 0, 1, 1, 1, 1, 1],
    'Edad': [19, 21, 20, 23, 31, 22, 35],
    'IngresosAnuales': [15, 15, 16, 16, 17, 17, 18],
    'NivelGasto': [39, 81, 6, 77, 40, 76, 6]
	}

#INTRODUCE LA LISTA DE VARIABLES QUE INTERVIENEN EN EL MODELO
columnOrder = ['co_cliente', 'Sexo', 'Edad', 'IngresosAnuales', 'NivelGasto']


#INTRODUCE EL DICCIONARIO CLUSTERS CLIENTES
dict_prob = {
	'cluster': [4, 3, 4, 3, 4, 3, 4],
    'co_cliente': [1000, 1001, 1002, 1003, 1004, 1005, 1006]
}

modelTest = TestRunner(variablesPath, environmentPath, modelID, sessionID, dict_vars, columnOrder, dict_prob)
modelTest.run()
