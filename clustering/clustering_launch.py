# -*- coding: utf-8 -*-
################################################################################
# Import libraries
################################################################################
from clustering_core import *
import sys
import inspect, os

################################################################################
# Get config path, parameters path, session ID
################################################################################
variablesPath = os.path.abspath(argv[1])
environmentPath = os.path.abspath(argv[2])
modelID = sys.argv[3]
sessionID = sys.argv[4]
try:
   targetDate= sys.argv[5]
   #"%Y-%m-%d"
except IndexError:
   targetDate=""

# modelExecution = ExecutionRunner(variablesPath, environmentPath, modelID, sessionID, targetDate, query)
modelExecution = ExecutionRunner(variablesPath, environmentPath, modelID, sessionID, targetDate)
modelExecution.run()
