import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import cPickle as pickle

dataset = pd.read_csv("clientes.csv")
X = dataset.iloc[:, [3,4]].values

from sklearn.cluster import KMeans
wcss = []
for i in range(1, 11):
    kmeans = KMeans(n_clusters = i, init = "k-means++", max_iter = 300, n_init = 10, random_state = 0)
    kmeans.fit(X)
    wcss.append(kmeans.inertia_)

plt.plot(range(1,11), wcss)
plt.title("Elbow Curve")
plt.xlabel("Número de Clusters")
plt.ylabel("WCSS(k)")
plt.show()

kmeans = KMeans(n_clusters = 5, init="k-means++", max_iter = 300, n_init = 10, random_state = 0)
y_kmeans = kmeans.fit_predict(X)

plt.scatter(X[y_kmeans == 0, 0], X[y_kmeans == 0, 1], s = 100, c = "red", label = "A")
plt.scatter(X[y_kmeans == 1, 0], X[y_kmeans == 1, 1], s = 100, c = "blue", label = "B")
plt.scatter(X[y_kmeans == 2, 0], X[y_kmeans == 2, 1], s = 100, c = "green", label = "C")
plt.scatter(X[y_kmeans == 3, 0], X[y_kmeans == 3, 1], s = 100, c = "cyan", label = "D")
plt.scatter(X[y_kmeans == 4, 0], X[y_kmeans == 4, 1], s = 100, c = "magenta", label = "E")
plt.scatter(kmeans.cluster_centers_[:,0], kmeans.cluster_centers_[:,1], s = 300, c = "yellow", label = "Centroides")
plt.title("Clúster de clientes")
plt.xlabel("Ingresos Anuales")
plt.ylabel("Nivel de Gasto")
plt.legend()
plt.show()

frame = pd.DataFrame(X)
frame['cluster'] = y_kmeans
clientes_cluster = frame['cluster'].value_counts()

n_clusters = 5

pickle.dump(kmeans, open("clustering_modelo.pkl", "wb"),-1)

# a,b = pickle.load(open("clustering_modelo.pkl","rb"))

# print(a,b)
# print (b[1])