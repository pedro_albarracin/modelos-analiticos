# -*- coding: utf-8 -*-
################################################################################
# Import libraries
################################################################################
from timeit import default_timer as timer
from pyspark import SparkConf, SparkContext
from pyspark.sql import HiveContext 
from pyspark.sql import Row 
from pyspark.sql.types import *
from pyspark.sql.functions import lit
from sys import argv,exit 
from string import Template
from logger import *
from datetime import date, datetime, timedelta
from calendar import monthrange
from abc import ABCMeta, abstractmethod
from functools import partial
from dateutil.relativedelta import relativedelta
import time
import csv
import ConfigParser 
import logging 
import cPickle as pickle
import sys
import json
import inspect, os
import pandas as pd

################ HELPER FUNCTIONS OUTSIDE ANY CLASS ############################
################################################################################
# function to calculate monthly date of execution
################################################################################
def calculateDateExecution(delta):
    today = date.today()
    lastMonth = today.replace(day=1) - relativedelta(months=delta-1) - timedelta(days=1)
    return lastMonth.strftime("%Y%m%d")

################################################################################
# function to calculate forced date of execution
################################################################################
def calculateForcedDateExecution(forcedate):
    targetdate = datetime.strptime(forcedate, "%Y-%m-%d")
    return date(targetdate.year, targetdate.month, monthrange(targetdate.year, targetdate.month)[1]).strftime("%Y%m%d")

################################################################################
# function to calculate daily date of execution
################################################################################
def calculateDailyDateExecution(delta):
    now = date.today()
    dailyExecution = now - timedelta(days=delta)
    return dailyExecution.strftime("%Y%m%d")

################################################################################
# function to predict on nodes
################################################################################
class MatrixPredictor(object):
    
    def __init__(self, model_object):
        self.model_object = model_object

    def __call__(self, partition):
        temp_label = []
        temp_features = []
        for i in partition:
            temp_label.append(i[0])
            temp_features.append(i[1:])
        
        predictions = self.model_object.predict(temp_features) if len(temp_features)>0 else []
        return iter([(x,y[1],'{:.15f}'.format(float(y[1]))) for x, y in zip(temp_label, predictions)])

class Model(object):
    __metaclass__ = ABCMeta
    appName = ""
    pipeline = ""
    
    @abstractmethod
    def run(self):
        pass

    def __init__(self, variablesPath, environmentPath, modelID, sessionID):
        self.modelID = modelID
        self.sessionID = sessionID
        self.variablesPath = variablesPath
        self.environmentPath = environmentPath

    def _loadModel(self, model_path, msg):
        try:
            self.logger.setCategory("INPUT")
            model_file = open(model_path, 'rb') 
            modelo_obj = pickle.load(model_file)
            model_file.close()
        except Exception, e:
            self._log_error(e, "No se ha cargado el modelo "+msg+" correctamente", 8)
        
        self.logger.printLog("INFO", "Modelo "+msg+" cargado correctamente desde la ruta: "+model_path)
        return modelo_obj

    def _loadEnviroment(self):
        try:
            # Spark context
            self.sc = SparkContext(appName = self.appName+self.Nmodel)
            self.sc.setLogLevel("ERROR")
        except Exception, e:
            self._log_error(e, "Error al crear contexto Spark", 6)
        try:
            # Hive Context
            self.sqlContext = HiveContext(self.sc)
        except Exception, e:
            self._log_error(e, "Error al crear contexto Hive", 6)

    def _setHiveConf(self):
        try:
            self.sqlContext.setConf("hive.exec.dynamic.partition", "true")
            self.sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
            sql = "create external table if not exists {0} (co_cliente bigint, prediccion float) partitioned by (modelID string, sessionID string)".format(self.outputDB+'.'+self.outputTable)
            self.sqlContext.sql(sql)
        except Exception, e:
            self._log_error(e, "Error al configurar tabla de salida", 6)
        
    def _modelExecutor(self, dataFrame, modelo_obj, msg):
        try:
            # Broadcasting Model and Predict on nodes
            self.logger.setCategory("SCORING")
            
            predictor = MatrixPredictor(modelo_obj)
            self.sc.broadcast(predictor)
            prob = dataFrame.mapPartitions(predictor)
        except Exception, e:
            self._log_error(e, "Error al ejecutar el scoring "+msg, 9)
        
        self.logger.printLog("INFO", "Se ha realizado el scoring "+msg+" correctamente")
        return prob
    
    ####### Helper to log without too much boilerplate
    def _log_error(self, exception, message, level):
        self.logger.printLog("ERROR", message)
        self.logger.printLog("ERROR", str(exception))
        self.logger.setAppName("BDMODELOS.RC")
        self.logger.setFormatter('[%(appname)s]=%(message)s')
        self.logger.printLog("ERROR", str(level))
        self.logger.setAppName("BDMODELOS.MSG")
        self.logger.printLog("ERROR", "MODEL"+self.Nmodel+" "+message )
        sys.exit(level)

class MonthlyModel(Model):
    __metaclass__ = ABCMeta

    def __init__(self, variablesPath, environmentPath, modelID, sessionID):
        super(MonthlyModel, self).__init__(variablesPath, environmentPath, modelID, sessionID)

    def _get_args(self):
        try:
            config = ConfigParser.RawConfigParser()
            config.read(self.variablesPath)
            config.read(self.environmentPath)
    
            self.tabla = config.get('Global', 'tabla')
            horizonte_prediccion = config.get('Global', 'horizonte')
            if self.targetDate == "":
              self.fecha = calculateDateExecution(int(horizonte_prediccion))
            else:
              self.fecha = calculateForcedDateExecution(self.targetDate)
            
            self.Nmodel = config.get('Global', 'Nmodel')
                        
            familia = config.get('Global', 'familia')
            subfamilia = config.get('Global', 'subfamilia')
            finalidad = config.get('Global', 'finalidad')
            
            workingDir = config.get('environment', 'workingDir')
            outputDir = config.get('environment', 'outputDir')
            self.outputFiles = config.get('environment', 'outputFiles')
            
            self.pathin =  workingDir + familia + '/' + subfamilia + '/' + finalidad + '/' + self.modelID + '/'
            self.pathout =  outputDir
            
            #Ruta del pkl del modelo
            self.model_path = self.pathin+'modelo'+self.Nmodel+'.pkl'
            if not os.path.isfile(self.model_path):
                self.model_path = workingDir +'modelo'+self.Nmodel+'.pkl'
            #Ruta del fichero del log de ejecucion
            self.log_name = self.pathout+self.modelID+'_'+self.sessionID+'.log'
            #Ruta del json de resultados de ejecucion
            self.jsonPath = self.pathout+self.modelID+'_'+self.sessionID+'.json'
            #Ruta del inventory control de ejecucion
            self.IcPath = self.pathout+self.modelID+'_'+self.sessionID+'.ic'
            #Ruta del fichero de puntuacion de ejecucion
            self.csvPath = self.outputFiles+self.modelID+'_'+self.sessionID+'.csv'
            
            self.outputDB = config.get('environment', 'outputDB')
            self.outputTable = config.get('Global', 'outputTable')

            self.logger = Log_Executor(self.log_name, "BDMODELOS.LOG", "INIT", self.modelID, self.pipeline, self.Nmodel)
            self.logger.printLog("INFO", "Inicio Log File")
            self.logger.printLog("INFO", "Ficheros de configuracion leidos correctamente")
            
            self.logger.printLog("INFO", "Empieza proceso de scoring para la fecha "+self.fecha)

            self.shortName=config.get("Global","short_model_id")
        except Exception, e:
            print '[BDMODELOS.LOG] '+time.strftime("%Y-%m-%d %H:%M:%S")+' ERROR [Model: - Pipeline: '+self.pipeline+' - Phase: INIT]: Error leyendo argumentos de entrada'
            print '[BDMODELOS.LOG] '+time.strftime("%Y-%m-%d %H:%M:%S")+' ERROR [Model: - Pipeline: '+self.pipeline+' - Phase: INIT]: '+str(e)
            print '[BDMODELOS.RC]=5'
            print '[BDMODELOS.MSG]=Error leyendo argumentos de entrada'
            sys.exit(5)

class TestRunner(MonthlyModel):
    appName = "Test Model"
    pipeline = "TEST_SCORING"

    def __init__(self, variablesPath, environmentPath, modelID, sessionID, dict_vars, columnOrder, dict_prob):
        super(TestRunner, self).__init__(variablesPath, environmentPath, modelID, sessionID)
        self.dict_vars = dict_vars
        self.columnOrder = columnOrder
        self.dict_prob = dict_prob
        #Al heredar el runner del MonthlyModel necesita asignar el targetDate, aunque no sea necesario para los tests
        self.targetDate = ""

    def _dfCreator(self, dict_vars, columnOrder):
        try:
            self.logger.setCategory("INPUT")
            dataFrame_vars = pd.DataFrame.from_dict(dict_vars)
            dataFrame = self.sqlContext.createDataFrame(dataFrame_vars[columnOrder])
        except Exception, e:
            self._log_error(e, "Error al leer los datos de variables", 6)

        self.logger.printLog("INFO", "Lectura de los datos de variables realizada correctamente")
        return dataFrame

    def run(self):
        start = timer()
        self._get_args()
        modelo_obj = self._loadModel(self.model_path, "mensual")
        self._loadEnviroment()
        dataFrame = self._dfCreator(self.dict_vars, self.columnOrder)
        
        prob = self._modelExecutor(dataFrame, modelo_obj, "mensual")
        
        dataFrame_test = pd.DataFrame(prob.collect(), columns = ['co_cliente', 'prediccion']).set_index(['co_cliente'])
        
        dataFrame_prob = pd.DataFrame.from_dict(self.dict_prob).set_index(['co_cliente'])
        end = timer()
        tiempo_ejecucion = end - start 

        self.logger.setCategory("ASSESSMENT")
        if round(dataFrame_test['prediccion'].mean(), 5) == round(dataFrame_prob['prediccion'].mean(), 5):
            self.logger.printLog("INFO", "Se ha comprobado la validez de la ejecucion correctamente")
            self.logger.setAppName("BDMODELOS.RC")
            self.logger.setFormatter('[%(appname)s]=%(message)s')
            self.logger.printLog("INFO", "0")
            self.logger.setAppName("BDMODELOS.MSG")
            self.logger.printLog("INFO", "Test del MODEL"+self.Nmodel+" finalizado correctamente en "+str(tiempo_ejecucion)+" seg")
            sys.exit(0)
        else:
            self.logger.printLog("ERROR", "Error al comprobar la validez de los datos por el proceso de scoring")
            self.logger.setAppName("BDMODELOS.RC")
            self.logger.setFormatter('[%(appname)s]=%(message)s')
            self.logger.printLog("ERROR", "11")
            self.logger.setAppName("BDMODELOS.MSG")
            self.logger.printLog("ERROR", "MODEL"+self.Nmodel+" error al comprobar la validez de los datos por el proceso de scoring")
            sys.exit(11)

class ExecutionRunner(MonthlyModel):
    appName = "Deploy Model"
    pipeline = "SCORING"
    
    def __init__(self, variablesPath, environmentPath, modelID, sessionID, targetDate, query):
        super(ExecutionRunner, self).__init__(variablesPath, environmentPath, modelID, sessionID)
        self.query = query
        self.targetDate = targetDate
    
    def _qryExecutor(self, query, msg):
        try:
            self.logger.setCategory("INPUT")
            qryText = query.substitute(dict(tabla=self.tabla, fecha=self.fecha))
        except Exception, e:
            self._log_error(e, "Error al construir la query " + msg, 6)

        self.logger.printLog("INFO", "Construccion de Query " + msg +" realizada correctamente")

        try:
            self.logger.setCategory("INPUT")
            dataFrame = self.sqlContext.sql(qryText)
        except Exception, e:
            self._log_error(e, "Error al ejecutar la query " + msg, 6)
        self.logger.printLog("INFO", "Ejecucion de la Query " + msg +" realizada correctamente")
        return dataFrame
        
    ################################
    def _loadExecutor(self):
    
        df = pd.read_csv("datos.csv", header=0)
    
        return df
    
    ################################
	 
    def _orderDataFrame(self, prob):
        try:
            self.logger.setCategory("OUTPUT")
            id_prob = prob.collect()
            dataframe_pandas = pd.DataFrame(id_prob, columns = ['co_cliente', 'prediccion']).set_index(['co_cliente'])
            dataframe_pandas.sort_values(['VD'], axis=0, ascending=False, inplace=True)
            return dataframe_pandas
        except Exception, e:
            self._log_error(e, "Error al transformar a Pandas DataFrame", 10)
            
        self.logger.printLog("INFO", "Se ha transformado a pandas DataFrame correctamente")
	
	def _calculate_regresion(self, dataframe_pandas):
        try:
            Media = dataframe_pandas['prediccion'].mean()
			Minimo = dataframe_pandas['prediccion'].min()
			Maximo = dataframe_pandas['prediccion'].max()
			Desviacion = dataframe_pandas['prediccion'].std()
        
            modelo_json = {
                "modelID": self.modelID,
                "sessionID": self.sessionID,
                "KPI": [
                    {
                        "kpiName": "Media",
                        "kpiType": "number",
                        "kpiValue": Media
                    },
                    {
                        "kpiName": "Minimo",
                        "kpiType": "number",
                        "kpiValue": Minimo
                    },
                    {
                        "kpiName": "Maximo",
                        "kpiType": "number",
                        "kpiValue": Maximo
                    },
                    {
                        "kpiName": "Desviacion",
                        "kpiType": "number",
                        "kpiValue": Desviacion
                    }
                ]
            }
            return modelo_json
        except Exception, e:
            self._log_error(e,  "Error al crear JSON de KPIs con un dataframe de longitud: "+str(num_clientes), 10)
    
    def _createJSONOutput(self, modelo_json):
        try:
            self.logger.setCategory("OUTPUT")
            with open(self.jsonPath, 'w') as file:
                json.dump(modelo_json, file)
        except Exception, e:
            self._log_error(e, "Error al generar JSON de KPIs en la ruta: "+self.jsonPath, 10)
            
        self.logger.printLog("INFO", "Se ha generado JSON de KPIs en la ruta: "+self.jsonPath)
    
    def _createHIVEOutput(self, prob):
        try:
            self.logger.setCategory("OUTPUT")
            # By http://spark.apache.org/docs/1.6.0/api/python/pyspark.sql.html#pyspark.sql.DataFrameWriter.insertInto
            # And https://spark.apache.org/docs/2.0.0/api/java/org/apache/spark/sql/DataFrameWriter.html#insertInto(java.lang.String)
            # We need to have the very same order of the columns than the table, as it ignores the column names. And 
            # it introduces wrong data. Even if we are creating the table, it seems that it does not work with a hardcoded columns
            # list, so we just query the table for it            
            sql = "SELECT * FROM {0} LIMIT 1".format(self.outputDB+'.'+self.outputTable)
            columns = self.sqlContext.sql(sql).columns

            prob_to_spark_types = prob.map(lambda x: Row(x[0],x[1].item(),x[2]))
            def_df = self.sqlContext.createDataFrame(prob_to_spark_types, ['co_cliente', 'prediccion']).withColumn('modelID', lit(self.shortName)).withColumn('sessionID', lit(self.fecha)).select(columns)
            
            def_df.write.partitionBy('modelID','sessionID').insertInto(self.outputDB+'.'+self.outputTable, overwrite=True)
        except Exception, e:
            self._log_error(e, "No se ha exportado la tabla de scoring en HIVE: "+self.outputDB+'.'+self.outputTable, 10)  
        self.logger.printLog("INFO", "se ha exportado la tabla de scoring en HIVE: "+self.outputDB+'.'+self.outputTable) 
    
    # Not used anymore
    def _createHDFSOutput(self, prob):
        try:
            self.logger.setCategory("OUTPUT")
            def_rd = prob.map(lambda x: Row(str(x[0])+"^"+str(x[1])+"^"+str(x[2]))).repartition(1)
            esquema = StructType([StructField("def", StringType(), True)])
            def_df = self.sqlContext.createDataFrame(def_rd, esquema)
            def_df.write.save(path=self.csvPath, format="text", mode="overwrite")
        except Exception, e:
            self._log_error(e, "No se ha exportado el fichero CSV de scoring en el path HDFS: "+self.csvPath, 10)
            
        self.logger.printLog("INFO", "Se ha ejecutado la exportacion del fichero CSV de scoring en el path HDFS: "+self.csvPath)        
    
    def _createIControlOutput(self):   
        try:
            texto = "name,partition1,partition2,partition3,logicTime\n" +self.outputTable+","+self.shortName+","+self.fecha+",,"+datetime.today().strftime('%Y%m%d')
            with open(self.IcPath,"w") as file: 
                file.write(texto) 
        except Exception, e:
            self._log_error(e, "Error al generar Inventory Control File: "+self.IcPath, 10)
            
        self.logger.printLog("INFO", "Se ha generado Inventory Control en la ruta: "+self.IcPath)
    
    def run(self):
        start = timer()
        self._get_args()
        modelo_obj = self._loadModel(self.model_path, "mensual")
        self._loadEnviroment()
        self._setHiveConf()
        
        # dataFrame = self._qryExecutor(self.query, "mensual")
        ############################
        # _loadExecutor devuelve un dataframe a partir de los datos cargados externamente, por ejemplo CSV
		dataFrame = self._loadExecutor()
        ####################################
        
		# prob = self._modelExecutor(dataFrame, modelo_obj, self.valor_desbalanceo, "mensual")
        prob = self._modelExecutor(dataFrame, modelo_obj, "mensual")
		df_sorted = self._orderDataFrame(prob)
		regresion = self._calculate_regresion(df_sorted)
		self._createJSONOutput(regresion)
		self._createIControlOutput()
		self._createHIVEOutput(prob)
        end = timer()
        tiempo_ejecucion = end - start
        self.logger.setAppName("BDMODELOS.RC")
        self.logger.setFormatter('[%(appname)s]=%(message)s')
        self.logger.printLog("INFO", "0")
        self.logger.setAppName("BDMODELOS.MSG")
        self.logger.printLog("INFO", "Scoring del MODEL"+self.Nmodel+" finalizado correctamente en "+str(tiempo_ejecucion)+" seg")
        sys.exit(0)

