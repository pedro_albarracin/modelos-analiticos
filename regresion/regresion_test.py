# -*- coding: utf-8 -*-
################################################################################
# Import libraries
################################################################################
from regresion_core import *
import sys
import inspect, os

################################################################################
# Get config path, parameters path, session ID
################################################################################
variablesPath = os.path.abspath(argv[1])
environmentPath = os.path.abspath(argv[2])
modelID = sys.argv[3]
sessionID = sys.argv[4]

#INTRODUCE EL DICCIONARIO DE LAS VARIABLES INDEPENDIENTES
dict_vars = {
	'co_cliente': [1000, 1325, 1801, 2050],
    'experiencia': [1.1, 1.3, 1.5, 2.0]
	}

#INTRODUCE LA LISTA DE VARIABLES INDEPENDIENTES QUE INTERVIENEN EN EL MODELO
columnOrder = ['co_cliente', 'experiencia']

#INTRODUCE EL DICCIONARIO DE LA PUNTUACION OBTENIDA
dict_prob = {
	'prediccion': [37096.72, 38965.91, 40835.10],
	'co_cliente': [1000, 1325, 1801, 2050]
}

modelTest = TestRunner(variablesPath, environmentPath, modelID, sessionID, dict_vars, columnOrder, dict_prob)
modelTest.run()
