# Regresión Lineal Simple

# Importamos las librerías
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sklearn
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
import cPickle as pickle

# Creamos el dataset a partir de un dict de python
matrixDatos={}
matrixDatos['co_user']=[1000,1325,1801,2050,1010,1018,1147,1002,1658,1998,1644,1386,1222,1241,1455,1023,1069,1078,1888,2385,2669,3120,2855,1401,1774,1697,1701,1653,1963,2211]
matrixDatos['VI']=[1.1,1.3,1.5,2.0,2.2,2.9,3.0,3.2,3.2,3.7,3.9,4.0,4.0,4.1,4.5,4.9,5.1,5.3,5.9,6.0,6.8,7.1,7.9,8.2,8.7,9.0,9.5,9.6,10.3,10.5]
matrixDatos['VD']=[39343.00,46205.00,37731.00,43525.00,39891.00,56642.00,60150.00,54445.00,64445.00,57189.00,63218.00,55794.00,56957.00,57081.00,61111.00,67938.00,66029.00,83088.00,81363.00,93940.00,91738.00,98273.00,101302.00,113812.00,109431.00,105582.00,116969.00,112635.00,122391.00,121872.00]

# Creamos el dataset cargando los datos desde CSV
datasetcsv = pd.read_csv("datos.csv", header=0)
X = datasetcsv.iloc[:, 1].values
y = datasetcsv.iloc[:, -1].values


datasetdict = pd.DataFrame(matrixDatos)
X = datasetdict.iloc[:, 1].values
y = datasetdict.iloc[:, -1].values

# Dividimos el dataset en conjunto de entrenamiento y conjunto de testing
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1/3, random_state = 0)

X_train=X_train.reshape(-1,1)
# Creamos el modelo de Regresión Lineal Simple con el conjunto de datos de entrenamiento

regression = LinearRegression()
regression.fit(X_train, y_train)

# Predecimos el conjunto de test
X_test=X_test.reshape(-1,1)
y_pred = regression.predict(X_test)

pickle.dump(regression, open("regresion_modelo.pkl", "wb"),-1)

loaded_model = pickle.load(open("regresion_modelo.pkl", 'rb'))

prediccion = loaded_model.predict(X_test)

