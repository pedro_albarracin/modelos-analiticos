# -*- coding: utf-8 -*-
################################################################################
# Import libraries
################################################################################
from series_core import *
import sys
import inspect, os

################################################################################
# Get config path, parameters path, session ID
################################################################################
variablesPath = os.path.abspath(argv[1])
environmentPath = os.path.abspath(argv[2])
modelID = sys.argv[3]
sessionID = sys.argv[4]

#INTRODUCE EL DICCIONARIO DE LAS VARIABLES DE LOS CLIENTES
dict_vars = {
	'Date': ["1968-01-01", "1968-02-01", "1968-03-01", "1968-04-01", "1968-05-01", "1968-06-01", "1968-07-01", "1968-08-01", "1968-09-01", "1968-10-01"],
	'Unrate': [3.7, 3.8, 3.7, 3.5, 3.5, 3.7, 3.7, 3.5, 3.4, 3.4]
	}

#INTRODUCE LA LISTA DE VARIABLES QUE INTERVIENEN EN EL MODELO
columnOrder = ['Date', 'Unrate']

#INTRODUCE EL DICCIONARIO DE LA PUNTUACION DE LOS CLIENTES
dict_prob = {
	'prediccion': [4.385603, 4.260501, 4.372299, 4.188201, 4.272706, 4.195072, 4.270626, 4.316875, 4.195257, 4.201229],
	'Date': ["1968-01-01", "1968-02-01", "1968-03-01", "1968-04-01", "1968-05-01", "1968-06-01", "1968-07-01", "1968-08-01", "1968-09-01", "1968-10-01"]
}

modelTest = TestRunner(variablesPath, environmentPath, modelID, sessionID, dict_vars, columnOrder, dict_prob)
modelTest.run()
