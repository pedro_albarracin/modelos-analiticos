
from matplotlib import pyplot
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
import pandas as pd
from datetime import datetime
import pickle
 

# Parseamos el campo fecha si fuera necesario, en este caso no lo es pero mantenemos la función
# En este caso será necesario incluir los parámetros parse_dates y date_parser en la función read_csv
def parser(x):
	return datetime.strptime(''+x, '%Y-%m-%d')
 
# Cargamos los datos del csv
# La columna 0 es la que contiene las fechas, squeeze: los datos parseados sólo están en una columna, date_parser: llama a la función parser
series = pd.read_csv('datos_serie.csv', header=0, parse_dates=[0], index_col=0, squeeze=True, date_parser=parser)

X = series.values

# Establecemos los datos que se usarán para train y test
size = int(len(X) * 0.66)
train, test = X[0:size], X[size:len(X)]

history = [x for x in train]
predictions = list()

for t in range(len(test)):
	################################################
	# Configuración del modelo ARIMA
	# Parámetros: p, d, q
	################################################
	model = ARIMA(history, order=(5,1,0))
	# No mostramos información de depuración de ajuste con disp=0
	model_fit = model.fit(disp=0)
	output = model_fit.forecast()
	yhat = output[0]
	predictions.append(yhat)
	obs = test[t]
	history.append(obs)
	print('predicted=%f, expected=%f' % (yhat, obs))
error = mean_squared_error(test, predictions)
print('Test MSE: %.3f' % error)

# Resumen de estadísticos del modelo
print(model_fit.summary())

# Mostramos la gráfica
pyplot.plot(test)
pyplot.plot(predictions, color='red')
pyplot.show()

pickle.dump(model, open("series_modelo.pkl", "wb"),-1)